# croqus

## purpose

for croquis.
this app double as animation tool

---

## suggest
use some pen-tablet device.

---

## how to use
draw something. also press next button.
animate bunch of these pictures.

## change history

* 0.1.1 - change ui
* 0.1.0 - use materializecss
* 0.0.6 - show guide by choose thumbnail
* 0.0.5 - sepalate NG
* 0.0.4 - sortable thumbnails
* 0.0.3 - thumbnail minimize
* 0.0.2 - next button implemented
* 0.0.1 - live reload (about develop)
* 0.0.0 - first commit

## TODO
UI layout
UX Design


