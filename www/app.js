var countMax    = 2000;
var count       = countMax;
var deltaCount  = 1;
var lines       = [];
var currentLine = [];
var prevLines   = [];
var width       = 400;
var height      = 400;

var animList    = document.getElementById('imgList');
var dustList    = document.getElementById('dustList');
var canvas      = document.getElementById('myCanvas');
var animCanvas  = document.getElementById('animCanvas');
var slider      = document.getElementById('myRange');
var nextButton  = document.getElementById('nextButton');
var clearButton = document.getElementById('clearButton');
var stopButton  = document.getElementById('stopButton');
var playButton  = document.getElementById('playButton');
var bakeButton  = document.getElementById('bakeButton');
var helpButton  = document.getElementById('helpButton');


canvas.addEventListener('mousemove', onMove,  false);
canvas.addEventListener('mousedown', onClick, false);
canvas.addEventListener('mouseup',   drawEnd, false);
canvas.addEventListener('mouseout',  drawEnd, false);

var sortableParam = {
  group: "omega",
  scroll: true,
  onChoose: (evt) => { 

  // 青い線でキャンバスに描いている
    for(line of evt.item.lines)
    {
      ctx.beginPath();
      ctx.lineWidth = 1
      for(p of line)
      {
        ctx.lineTo(p.X, p.Y);
      }
      ctx.strokeStyle = "#00FFFF";
      ctx.stroke();    
    }

  },
};

Sortable.create(animList, sortableParam);
Sortable.create(dustList, sortableParam);

helpButton.onclick = () =>
{
  M.toast({html: '使い方が出る予定'});
}

clearButton.onclick = () =>
{
  M.toast({html: 'キャンバスがクリアされる予定'});
}

nextButton.onclick = () =>
{
  count = 0;
};

stopButton.onclick = () =>
{
  deltaCount  = deltaCount  == 0 ? 1 : 0;

  var msg = deltaCount == 0 ? 'ポーズ中' : '再開';
  M.toast({html: msg});

};

bakeButton.onclick = () =>
{
  M.toast({html: 'gifアニメが生成される予定なのだがちょいまたれよ'});
  return;

  gifAnimation = new GIFEncoder();
  gifAnimation.setDelay(0);
  gifAnimation.start();
  var context = canvas.getContext("2d");
  gifAnimation.addFrame( context );
  gifAnimation.finish();
  var byteString = gifAnimation.stream().getData() ;
  var ab = new ArrayBuffer(byteString.length);
  var ia = new Uint8Array(ab);
  for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
  }

  var blob = new Blob( [ab], {type: "image/gif" });

  var a = document.createElement("a");
  a.href = window.URL.createObjectURL( blob );

  a.download = "animation.gif";
  a.innerHTML = "gifアニメーション";

  document.getElementsByTagName( "body" )[0].appendChild(a);

};

var animCtx = animCanvas.getContext('2d');

var ctx = canvas.getContext('2d');
ctx.beginPath();
ctx.fillStyle = "#f5f5f5";
ctx.fillRect(0, 0, 700, 400);

function onMove(e)
{
  if (e.buttons === 1 || e.witch === 1) {
    var rect = e.target.getBoundingClientRect();
    var X = ~~(e.clientX - rect.left);
    var Y = ~~(e.clientY - rect.top);
    draw(X, Y);
  };
};

function onClick(e)
{
  if (e.button === 0)
  {
    var rect = e.target.getBoundingClientRect();
    var X = ~~(e.clientX - rect.left);
    var Y = ~~(e.clientY - rect.top);
    draw(X, Y);
  }
};

function draw(X, Y)
{
  var p = {X:X, Y:Y};
  currentLine.push(p);
};

function drawEnd() 
{
  lines.push(currentLine);
  currentLine = [];  
}

function do_nothing(){}

function new_piece()
{
  M.toast({html: 'フレームが追加されました'});

  var png      = canvas.toDataURL();
  var image    = new Image();
  image.src    = png;
  image.width  = 100;
  image.height = 100;
  image.lines  = prevLines;
  animList.appendChild(image);

  count = countMax;
  ctx.beginPath();
  ctx.fillStyle = "#f5f5f5";
  ctx.fillRect(0, 0, 700, 400);

  // 青い線でキャンバスに描いている
  /*
  for(line of prevLines)
  {
    ctx.beginPath();
    ctx.lineWidth = 1
    for(p of line)
    {
      ctx.lineTo(p.X, p.Y);
    }
    ctx.strokeStyle = "#00FFFF";
    ctx.stroke();    
  }
  */

  prevLines = [];
}

function animloop()
{
  if(animList.children.length!=0)
  {
    var cnt = ~~(count / (60) * slider.value);
    index = animList.children.length - cnt % animList.children.length - 1;
    var img = animList.children[index];
    animCtx.drawImage(img, 0, 0);
    //animCtx.drawImage(imageList[index], 0, 0);
  }

  ctx.beginPath();
  ctx.strokeStyle = "#000000";
  ctx.rect(10, 10, count/10, 10);
  ctx.stroke();

  ctx.beginPath();
  ctx.lineWidth = 1
  for(p of currentLine)
  {
    ctx.lineTo(p.X, p.Y);
  }
  ctx.stroke();    

  for(line of lines)
  {
    ctx.beginPath();
    ctx.lineWidth = 1
    for(p of line)
    {
      ctx.lineTo(p.X, p.Y);
    }
    ctx.stroke();    
  }

  prevLines = lines.length != 0 ? prevLines.concat(lines)
                                : prevLines;
  lines = [];

  count -= deltaCount;
  if(count<=0)
  {
    count = countMax;

    var action = prevLines.length != 0 ? new_piece
                                       : do_nothing

    action();                                    
  }

  window.requestAnimationFrame(animloop);
}

window.onload = () => {
  animloop();
};


