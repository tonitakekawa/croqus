
const gulp = require('gulp')
const browserSync =require('browser-sync')
const src = './www'

const srcDir = {
  html:[src + '*.html'],
  css:[src + 'assets/css/*.css', src + 'assets/css/**/*.css'],
  js:[src + '*.js'],
}

gulp.task('html', () => {
  gulp.src(srcDir.html)
  .pipe(browserSync.reload({ stream:true }))
})

gulp.task('css', () => {
  gulp.src(srcDir.css)
  .pipe(browserSync.reload({ stream:true }))
})

gulp.task('browser-sync', () => {
  browserSync({
    server: {
       baseDir: src,
       index  : 'index.html'
    },
    open: 'external',
    port: 2000,
    browser:['google chrome'],
  })
})

//デフォルト
gulp.task('default', ['browser-sync'], () => {
  gulp.watch([srcDir.html, srcDir.css], ['html', 'css'])
})